# Гипотеза

<!-- Утверждение, которое мы будем обосновывать - "Состав микробиома зависит от среды обитания". -->

Для проверки гипотезы выполним стандартную процедуру анализа данных высокопроизводительного секвенирования проб, взятых из разных мест в оз. Байкал на оборудовании Illumina.

<!-- Координаты и глубина е известны. Задача становиться интереснее -->

<!-- Варианты условий -->

# Выполнение стандартной процедуры MiSeq SOP

Для выполнения исследования проведена вручную стандартная процедура анализа данных (SOP, Standard operational procedure), описанная в [SOP2013]. В качестве начальных данных Лимнологическим институтом СО РАН предоставлены данные секвенирования Illumina, представляющие собой склеенные последовательности: левые и правые прочтения объединены в одну последовательность. Затем, в них удалены штрих-коды, идентифицирующие прочтения в пробах. В связи с этим мы пропустили несколько начальных этапов ```make.file``` и ```make.cotigs```.

Сначала необходимо провести обзор общих свойств последовательностей. Для этого используется команда ```summary.seqs``` программы Mothur.

```bash
summary.seqs(fasta=HXH779K01.shhh.fasta)
```

Получаем сводную таблицу, отображающую характеристики групп последовательностей.

```text
Using 4 processors.

		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	1	207	207	0	3	1
2.5%-tile:	1	263	263	0	4	1124
25%-tile:	1	432	432	0	4	11234
Median: 	1	447	447	0	5	22468
75%-tile:	1	465	465	0	5	33701
97.5%-tile:	1	472	472	0	6	43811
Maximum:	1	503	503	0	9	44934
Mean:	1	434	434	0	4
# of Seqs:	44934

It took 0 secs to summarize 44934 sequences.
```

В таблице показано, что одна последовательность имеет длину 207 символов (пар оснований), 1123 - 263, 10110 - 432, 11234 - 447, 11233 - 465, 10110 - 472, 1123 - 503. В среднем, длина последовательностей - 434 символов. В этих данных последовательности содержат праймеры ("штрих-коды"), части последовательностей, расположенные по краям прочтений, относящие  последовательность к определенной пробе. Эти праймеры необходимо удалить, и при этом сохранить соответствие. Штрих-коды содержатся в файле ```oligos```.

```text
#U341F
forward	CCTACGGGRSGCAGCAG
#barcodes bacteria
barcode	ACGAGTGCGT	S12K
barcode	ACGCTCGACA	S3M
barcode	AGACGCACTC	SMS
```

Выполним операцию и просмотрим опять сводную таблицу, но уже об обрезанных последовательностях.

```bash
trim.seqs(fasta=HXH779K01.shhh.fasta, oligos=oligos, name=HXH779K01.shhh.names, pdiffs=2, bdiffs=1, minlength=100, processors=2)
```

<!--
HXH779K01.shhh.trim.fasta
HXH779K01.shhh.scrap.fasta
HXH779K01.shhh.trim.names
HXH779K01.shhh.scrap.names
HXH779K01.shhh.groups
-->

```bash
summary.seqs(fasta=HXH779K01.shhh.trim.fasta)
```

В результате получаем следующую таблицу:

```text
Using 4 processors.

		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	1	198	198	0	3	1
2.5%-tile:	1	237	237	0	4	1112
25%-tile:	1	406	406	0	4	11112
Median: 	1	420	420	0	5	22223
75%-tile:	1	438	438	0	5	33334
97.5%-tile:	1	445	445	0	6	43333
Maximum:	1	476	476	0	9	44444
Mean:	1	407	407	0	4
# of Seqs:	44444

It took 0 secs to summarize 44444 sequences.
```

<!--
HXH779K01.shhh.trim.summary
-->

Теперь видим, что одна последовательность имеет длину 198 пар оснований, 1111 - 237, 10000 - 406, 11111 - 420, 11111 - 420, 9999 - 445, 1111 - 476. В среднем, длина последовательностей - 407 символов. Получается, что 1111 последовательностей, имеющих длину меньше 406 символов представляют собой, вероятно, неправильную склейку исходных пар прочтений: остальные 43333 имеют длины от 406 до 475 символов, т.е. их длина отличается не больше, чем на 16% (примерно 1/6 длины). Будем считать все последовательности, длина которых меньше 406 забракованными, их необходимо вырезать.

<!--
Вторым анализируемым критерием является ```Ambigs```, он показывает оценку неопределенности
-->

```bash
screen.seqs(fasta=HXH779K01.shhh.trim.fasta, group=HXH779K01.shhh.groups, summary=HXH779K01.shhh.trim.summary, minlength=406)
```

Результат выполнения команды (самая важная часть выдачи):

```text
It took 0 secs to screen 44444 sequences, removed 11052.

Running command: remove.seqs(accnos=HXH779K01.shhh.trim.bad.accnos.temp, group=HXH779K01.shhh.groups)
Removed 11052 sequences from your group file.
```

<!--
HXH779K01.shhh.trim.good.summary
HXH779K01.shhh.trim.good.fasta
HXH779K01.shhh.trim.bad.accnos
HXH779K01.shhh.good.groups
-->

Мы исключили 11052 "коротких" последовательностей и 11052 записи из файла соответствия прочтений номерам проб. Проверим еще раз общие статистические свойства оставшихся последовательностей.

```bash
summary.seqs(fasta=HXH779K01.shhh.trim.good.fasta)
```

```text
		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	1	406	406	0	3	1
2.5%-tile:	1	409	409	0	4	835
25%-tile:	1	420	420	0	4	8349
Median: 	1	423	423	0	5	16697
75%-tile:	1	442	442	0	5	25045
97.5%-tile:	1	446	446	0	6	32558
Maximum:	1	476	476	0	9	33392
Mean:	1	429	429	0	4
# of Seqs:	33392

It took 1 secs to summarize 33392 sequences.
```

Теперь у нас осталось 33392 последовательности, средняя длина которых равна 429 символам. Таким образом, все их длины являются примерно одинаковыми. Теперь можно переходить к поиску и учету уникальных последовательностей. Это делается для того, чтобы не обрабатывать одно и то же несколько раз подряд. Две последовательности являются одинаковыми, если их символы полностью совпадают.

```bash
unique.seqs(fasta=HXH779K01.shhh.trim.good.fasta)
```

<!--
HXH779K01.shhh.trim.good.names
HXH779K01.shhh.trim.good.unique.fasta
-->

После выполнения этой команды, у нас осталось 26744 уникальных последовательности. Обобщенные характеристики (средняя длина) не изменились. Теперь необходимо построить таблицу, показывающую сколько раз каждая копия входит в пробу.

```bash
count.seqs(name=HXH779K01.shhh.trim.good.names, group=HXH779K01.shhh.good.groups)
```

<!--
HXH779K01.shhh.trim.good.count_table
-->

В результате получаем большую таблицу, которую можем также исследовать при помощи команды ```summary.seqs```.

```bash
summary.seqs(count=HXH779K01.shhh.trim.good.count_table, fasta=HXH779K01.shhh.trim.good.unique.fasta)
```

Теперь в сводной таблице показано общее количество последовательностей и количество уникальных.

```
		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	1	406	406	0	3	1
2.5%-tile:	1	409	409	0	4	835
25%-tile:	1	420	420	0	4	8349
Median: 	1	423	423	0	5	16697
75%-tile:	1	442	442	0	5	25045
97.5%-tile:	1	446	446	0	6	32558
Maximum:	1	476	476	0	9	33392
Mean:	1	429	429	0	4
# of unique seqs:	26744
total # of seqs:	33392
```

Таким образом, в результате пройденных этапов мы отфильтровали подозрительные и дублирующие данные, а также подсчитали количество последовательностей в каждой пробе (группе).

## Выравнивание последовательностей

Основным этапом в биоинформатике является выравнивание. В результате чего генетические последовательности сопоставляются с последовательностями в референтных базах данных, где храниться информация об извесних генетических кодах микроорганизмов. Иными словами, теперь для наших последовательностей мы найдем наиболее близко соответствующий микроорганизм.

Для выполнения выравнивания нам нужны уникальные последовательности и база данных генетического кода микроорганизмов. Данный этап исследования является наиболее вычислительно трудным: занимает много времени на вычисления. На домашнем компьютере потребовалось 95 секунд на выравнивание 26744 последовательностей. Выравнивание осуществляется при помощи команды:

```bash
align.seqs(fasta=HXH779K01.shhh.trim.good.unique.fasta, reference=silva.bacteria.fasta)
```

<!--
HXH779K01.shhh.trim.good.unique.align
HXH779K01.shhh.trim.good.unique.align.report
HXH779K01.shhh.trim.good.unique.flip.accnos
-->

Далее проводим стандартную процедуру ```summary.seqs``` к результатам выравнивания и таблице встречаемости прочтений в образцах.

```bash
summary.seqs(fasta=HXH779K01.shhh.trim.good.unique.align, count=HXH779K01.shhh.trim.good.count_table)
```

```text
		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	6237	22529	5	0	1	1
2.5%-tile:	6428	22577	409	0	4	835
25%-tile:	6428	25292	420	0	4	8349
Median: 	6428	25292	423	0	5	16697
75%-tile:	6428	25292	442	0	5	25045
97.5%-tile:	6430	25293	446	0	6	32558
Maximum:	43107	43116	476	0	9	33392
Mean:	6435	25061	429	0	4
# of unique seqs:	26744
total # of seqs:	33392

It took 21 secs to summarize 33392 sequences.
```

Из таблицы получаем следующую информацию. Почти все последовательности (24210) выровнены в границах с 6428-го символа до 25292-го. Небольшое отклонение показали 832 и 7513. Самое больное отклонение от общей картины показывают 1 последовательность (6237-22529), соответствующая первой строке таблицы, и 834 последовательности в конце таблицы (43107-43116). Большие отклонения говорят об ошибках секвенирования, порождаемых секвенаторами. Кроме того, в таблице показано, что в последней строке найдены последовательности, включающие подряд 9 одинаковых гомополимера (Polymer), но в базе данных бактерий SILVA, нет последовательностей, включающих гомополимеры такой и большей длины. Поэтому, необходимо вырезать выравнивания, попадающие одним из концов в интервал 6428-25292, или полностью находящиеся вне этого интервала и имеющие ```Polymer>8``` [Roy2012].

```bash
screen.seqs(fasta=HXH779K01.shhh.trim.good.unique.align, count=HXH779K01.shhh.trim.good.count_table, summary=HXH779K01.shhh.trim.good.unique.summary, maxhomop=8, start=6428, end=25292)
```

<!--
HXH779K01.shhh.trim.good.pick.count_table

HXH779K01.shhh.trim.good.unique.good.summary
HXH779K01.shhh.trim.good.unique.good.align
HXH779K01.shhh.trim.good.unique.bad.accnos
HXH779K01.shhh.trim.good.good.count_table
-->

Данная команда удалила 5574 последовательности. В результате получаем следующую картину:

```bash
summary.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.align, count=HXH779K01.shhh.trim.good.good.count_table)
```

```text
		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	6237	25292	414	0	3	1
2.5%-tile:	6428	25292	418	0	4	686
25%-tile:	6428	25292	420	0	4	6857
Median: 	6428	25292	424	0	5	13714
75%-tile:	6428	25292	442	0	5	20570
97.5%-tile:	6428	25293	446	0	6	26741
Maximum:	6428	26152	476	0	8	27426
Mean:	6427	25295	430	0	4
# of unique seqs:	21170
total # of seqs:	27426

It took 6 secs to summarize 27426 sequences.
```

Теперь последовательности выровнены в одном интервале. Далее необходимо вырезать "выступающие" концы и пропуски, обозначенные при помощи символа ".".

```bash
filter.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.align, vertical=T, trump=.)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.fasta
-->

```text
Length of filtered alignment: 871
Number of columns removed: 49129
Length of the original alignment: 50000
Number of sequences used to construct filter: 21170
```

В результате оказывается, что изначально проводилось выравнивание относительно референтных последовательностей длиной 50000 символов, из которых 49129 не несли полезную информацию. В результате остались референцные последовательности длиной 871 символ. Данная операция из-за удаления пропусков порождает новые неуникальные последовательности, которые надо идентифицировать и учесть.

```bash
unique.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.filter.fasta, count=HXH779K01.shhh.trim.good.good.count_table)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.count_table
HXH779K01.shhh.trim.good.unique.good.filter.unique.fasta
-->

В результате количество уникальных последовательностей сократилось на 118 до 21052. Данный факт учитывается в таблице встречаемости. Еще одним способом исключения случайных ошибок секвенирования является фильтрация, основанная на анализе схожести каждой последовательности с некоторым "большинством": все множество разбивается на группы по количеству схожих, потом в порядке убывания попарно сравнивается с тестируемой последовательностью, и, если количество "мутаций" не больше, чем 1 на 100 базовых пар, то считать что эта последовательность неотличима от соответствующего "большинства", т.е. считать ее неуникальной.

Последовательности, которые мы изучаем имеют длину 431, т.е. содержат 4 полных сотни базовых пар, поэтому в следующей команде коэффициент ```diffs``` равен 4.

```bash
pre.cluster(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.fasta, count=HXH779K01.shhh.trim.good.unique.good.filter.count_table, diffs=4)
```

Количество последовательностей, считающихся уникальными, теперь 14234. Согласно методики MiSeq теперь необходимо переходить к поиску "химер", последовательностей, которые получились правдоподобным объединением правых и левых прочтений от разных ДНК. Химеры могут быть найдены и уничтожены при помощи следующих команд.

```bash
chimera.vsearch(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.fasta, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.count_table, dereplicate=t)

remove.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.fasta, accnos=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.accnos)
```

В результате были удалены химеры, получилось 11119 уникальных последовательности (всего - 27022), общее количество последовательностей сократилось на 1.3%, что допустимо по методике.

```bash
summary.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.fasta, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.count_table)
```

```text

		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	1	871	414	0	3	1
2.5%-tile:	1	871	418	0	4	676
25%-tile:	1	871	420	0	4	6756
Median: 	1	871	423	0	5	13512
75%-tile:	1	871	442	0	5	20267
97.5%-tile:	1	871	445	0	6	26347
Maximum:	1	871	459	0	8	27022
Mean:	1	871	430	0	4
# of unique seqs:	11119
total # of seqs:	27022
```

Рекомендуется также провести еще один шаг фильтрации, отсеивающий различные "ненужные" последовательности, например, митохондриальные, или не входящие в область наших интересов. Сначала проведем классификацию наших последовательностей при помощи предобученной байесовской сети для таксономии микроорганизмов. Готовая сеть предоставлена сотрудниками ЛИН СО РАН.

```bash
classify.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.fasta, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.count_table, reference=trainset9_032012.rdp.fasta, taxonomy=trainset9_032012.rdp.tax, cutoff=80)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.taxonomy
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.tax.summary
-->

Теперь можно удалить, например, все, что не является бактерией, т.е. эукариоты, ДНК в хлоропластах, митохондриальные ДНК, неклассифицированные микроорганизмы, и археи.

```bash
remove.lineage(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.fasta, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.count_table, taxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.taxonomy, taxon=Chloroplast-Mitochondria-unknown-Archaea-Eukaryota)
```

Однако нас интересует все микробактериальное сообщество, поэтому мы удалим только ДНК в хлоропластах и митохондриях.

```bash
remove.lineage(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.fasta, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.count_table, taxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.taxonomy, taxon=Chloroplast-Mitochondria)
```

```bash
summary.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.fasta,count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table)
```

В обоих случаях программа удалила 1828 уникальных последовательностей и 5728 их упоминаний в таблице встречаемости. Осталось 9291 уникальные последовательности, всего - 21294.

<!-- До этого момента все сделано правильно -->

# Анализ микробактериального сообщества

Так как мы намеревались исследовать изменение структуры популяции микроорганизмов (микробиома) в пробах, нам надо изучить, какие сколько в каждой пробе находятся прочтений, к каким видам они принадлежат (или с какими видами они наиболее схожи), а также изменение этих показателей от пробы к пробе. Первый вид анализа в NGS называется Альфа-разнообразие (Alpha-diversity), второе - Бета-разнообразие (Beta-diversity). Результаты удобно представлять в виде специальным образом размеченных диаграмм (филогенетических деревьев) и таблиц.

В наших данных получилось 21294 последовательностей, из которых 9291 уникальных. Первое число характеризует количество микроорганизмов в пробах, второе - видовое разнообразие. На самом деле, уникальные последовательности содержат последовательности, принадлежащие одному виду, но содержащие мутации, которые и делают их различимыми в рамках вида. Чтобы выделить виды среди мутаций производят таксономию объектов (прочтения), в результате которой последовательности распределяются на подмножества, где в кадом подмножестве находятся сходные объекты. Такие подмножества называются Операционные таксономические единицы (ОТИ) (Perational taxonomic units, OTU). OTU могут представлять собой вид или подвид, в зависимости от степени схожести элементов.

В рамках одного OTU последовательности можно считать теперь неразличимыми и сравнивать образцы наличием или отсутствием конкретного OTU, а также его количеством (количественный анализ). OTU позволяет дополнительно сократить сложность вычислений на дальнейших этапах анализа. OTU можно привязывать к существующему филогенетическому Древу Жизни, т.е. проводить качественный анализ проб.

## Определение OTU и их таксономии

<!--
Сначала необходимо провести попарное сравнение последовательностей, для этого выполним команду:

```bash
dist.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.fasta, cutoff=0.3)
```

HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.dist


Параметр ```cutoff``` указывает максимальное "отличие" между последовательностями, считающимися неразличимыми, они потом приписываются к одному OTU.

Построим таксономию наших OTU, в данном случае не зависящую от стандартной таксономии.

```bash
cluster(column=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.dist, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table)
```

HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.list
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.steps
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.sensspec

-->

Выделение OTU будем сразу проводить на основе данных, хранимых в базе данных SILVA. Данную операцию выполняет команда ```cluster.split```.

```bash
cluster.split(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.fasta, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table, taxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.taxonomy, splitmethod=classify, taxlevel=4, cutoff=0.03)
```
<!-- взяты только бактерии, убраны митохондрии и хлоропласты -->

<!--
Output File Names:
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.dist
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.list
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.sensspec
-->

В результате получаем список OTU с принадлежащими им последовательностями. Всего получилось 1001 OTU, т.е. в каждом OTU в среднем по 9 уникальных последовательностей. Теперь можно узнать, сколько и каких OTU находится в каждой пробе.

```bash
make.shared(list=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.list, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table, label=0.03)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.shared
-->

В результате получаем таблицу соответствия номера OTU идентификатору пробы. Теперь, наконец то, можно построить таксономию для каждой OTU!

```bash
classify.otu(list=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.list, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table, taxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.taxonomy, label=0.03)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.0.03.cons.taxonomy
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.opti_mcc.0.03.cons.tax.summary
-->


```text
OTU     Size    Taxonomy
Otu0001 2884    Otu001  2883    Bacteria(100);Bacteria_unclassified(100);Bacteria_unclassified(100);Bacteria_unclassified(100);Bacteria_unclassified(100);Bacteria_unclassified(100);
Otu002  1045    Bacteria(100);"Verrucomicrobia"(100);Spartobacteria(100);Spartobacteria_genera_incertae_sedis(100);Spartobacteria_genera_incertae_sedis_unclassified(100);Spartobacteria_genera_incertae_sedis_unclassified(100);
Otu003  1038    Bacteria(100);"Actinobacteria"(100);Actinobacteria(100);Actinomycetales(100);Actinomycetales_unclassified(100);Actinomycetales_unclassified(100);
Otu004  861     Bacteria(100);"Actinobacteria"(100);Actinobacteria(100);Acidimicrobiales(100);Acidimicrobiales_unclassified(76);Acidimicrobiales_unclassified(76);
Otu005  660     Bacteria(100);"Actinobacteria"(100);Actinobacteria(100);Acidimicrobiales(100);Acidimicrobiaceae(99);Ilumatobacter(99);
Otu006  544     Bacteria(100);"Acidobacteria"(100);Acidobacteria_Gp6(100);Gp6(100);Gp6_unclassified(100);Gp6_unclassified(100);
Otu007  497     Bacteria(100);"Verrucomicrobia"(100);Subdivision3(100);Subdivision3_genera_incertae_sedis(100);Subdivision3_genera_incertae_sedis_unclassified(100);Subdivision3_genera_incertae_sedis_unclassified(100);
Otu008  484     Bacteria(100);"Bacteroidetes"(100);Flavobacteria(100);"Flavobacteriales"(100);Flavobacteriaceae(100);Flavobacterium(100);
```

В таблице, например, OTU0005 встречается 660 раз в образцах, почти все последовательности (99%) принадлежат виду Ilumatobacter. Эту бактерию удалось выявить в морских донных отложениях и вырастить в специальных условиях, близких к морским, японским ученым А. Матсумото [mats2009]. Получается, что эта бактерия может существовать и в пресноводной среде оз. Байкал.

<img src="images/illumatobacter.jpg>

OTU0008 - это Flavobacterium....

Продолжим наши исследования.

## Филотипы (таксоны)

Распределим последовательности по OTU, эта информация понадобиться для построения дерева, показывающего количество микроорганизмов каждого вида в пробе (преобладающих видов). Выполним команду:

<!-- NOTE: DO NOT EDIT. PARAMETER is OK!!! -->

```bash
phylotype(taxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.taxonomy)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.sabund
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.rabund
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.list
-->

Параметр в следующей команде ```label``` показывает здесь интересующий нас уровень филума (типа), его значение меняется от 1 до 6, что соответствует виду и царству. Нам нужно все определить до вида, поэтому указываем 1.

```bash
make.shared(list=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.list, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table, label=1)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.shared
-->

Теперь получим таксономию каждого OTU при помощи команды ```classify.otu``` над нашими филотипами:

```bash
classify.otu(list=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.list, count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table, taxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.taxonomy, label=1)
```

<!-- NOTE. Таксономия OTU перестраивается (переименовывается) Дикость! -->

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.1.cons.taxonomy
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.1.cons.tax.summary
-->

## Филогенетика (дерево жизни)

Нам интересно на сколько сильно отличаются последовательности друг от друга в рамках одного OTU, генетические расстояния между OTU, то следующие команды дают нам необходимую информацию. Первая сравнивает уникальные последовательности, вторая - строит филогенетическое дерево.

```bash
dist.seqs(fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.fasta, output=lt)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.phylip.dist
-->

```bash
clearcut(phylip=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.phylip.dist)
```

<!--
HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.phylip.tre
-->

Какой-либо полезной информации для анализа условий среды в местах отбора проб следующий рисунок [phylothree] не несет, оно используется для первичного обзора структуры микробами, а также измерения генетического расстояния между отдельными уникальными последовательностями. Тем не менее его интересно построить, разметив ребра дерева информацией принадлежности последовательностей к конкретным OTU.


Рисунок [philotree] Филогенетическое дерево уникальных последовательностей.


## Сокращение имен файлов

Дальнейшие действия будет удобнее делать, если сократить длины файлов. Применим команду ```rename.file```.

```bash
rename.file(taxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.taxonomy, shared=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.shared)
```

<!--
Current files saved by mothur:
column=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.dist
fasta=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.fasta
list=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.list
phylip=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.phylip.dist
rabund=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.rabund
sabund=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.sabund
shared=HXH779K01.tx.shared
taxonomy=HXH779K01.taxonomy
constaxonomy=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.rdp.wang.pick.tx.1.cons.taxonomy
tree=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.pick.pick.phylip.tre
count=HXH779K01.shhh.trim.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table
processors=4
-->

Теперь можно посмотреть, сколько последовательностей находиться в каждом образце. Подсчитаем их по "группам" (образцам).

```bash
count.groups(shared=HXH779K01.tx.shared, group=HXH779K01.shhh.good.groups)
```

<!--
HXH779K01.tx.count.summary
-->

У нас получилось, что в одном из образцов находится всего 166 последовательностей. Маловато, но мы попробуем продолжить с таким количеством. Чтобы сравнивать образцы надо из образцов с большим количеством случайно выбирать 166 последовательностей. Это называется subsampling. Надо сгенерировать файл с одинаковым количеством для каждой пробы:

```bash
sub.sample(shared=HXH779K01.tx.shared, size=166)
```
<!-- # FIXME: М.б. увеличить это число???? в оригинале было 20хх. -->

Коэффициент покрытия везде оказался более 89%, что на этапе субсэмплинга выборка получилась хорошая.

<!--
HXH779K01.tx.1.subsample.shared
-->

## Альфа-разнообразие

Прейдем к анализу встречаемости последовательностей в пробах, Альфа-разнообразие. Сперва, сгенерируем оценки вхождения OTU в пробы.

```bash
rarefaction.single(shared=HXH779K01.tx.1.subsample.shared, calc=sobs, freq=100)
```

<!--
HXH779K01.tx.1.subsample.groups.rarefaction
-->


На рисунке allalha показано разнообразие встречаемости по первым наиболее часто встречающимся OTU (17 штук).

Рисунок [allalha] Альфа-разнообразие наиболее часто встречающихся OTU.

Теперь исключим OTU001, вид которого мы не смогли определить, а остальные 16 OTU заменим на их известную классификацию. На рисунке [otusalpha] определившиеся до вида бактерии показаны своим названием, остальные - таксонами разного уровня, уровень таксона приведен после точки (2-род, 3-семейство и т.д. до царства - 6).

Рисунок [otusalpha]. Альфа-разнообразие OTU, классифицированных в классе (уровень 5) и точнее.

Построим диаграмму Альфа-разнообразия для бактерий, виды которых нам удалось определить (рисунок [genusalpha]).


Рисунок [genusalpha] Альфа-разнообразие определенных конкретных видов бактерий (оси поменяны местами для удобства отображения информации).

<!-- Обсуждение результата -->

## Изучение взаимного соотношения (Beta diversity)

Сравним свойства проб (образцов) при помощи содержащихся в них OTU. Сначала сравним пробы по наличию OTU.

```bash
dist.shared(shared=HXH779K01.tx.1.subsample.shared, calc=thetayc-jclass, subsample=t)
```

<!--
HXH779K01.tx.1.subsample.thetayc.1.lt.ave.dist
HXH779K01.tx.1.subsample.thetayc.1.lt.std.dist
HXH779K01.tx.1.subsample.jclass.1.lt.ave.dist
HXH779K01.tx.1.subsample.jclass.1.lt.std.dist
-->

Полученные матрицы используем для классификации проб по схожести и проведения анализа Бета-разнообразия.

Итак, имеем файл с матрицей измерения степени отличия проб друг от друга, данные которой используем для графического представления соотношение и разнообразие в образцах (рисунок [compareprobes]). Рисунок показывает результаты анализа схожести проб в виде классификации (бинарной таксономии). На нем выделены 7 классов проб, названия которых раскрашены согласно классовой принадлежности.

Рисунок [compareprobes] Классификация мест отбора проб по биоразнообразию.

Интересно показать классификацию на карте озера Байкал (рисунок [baikal]). Обратим внимание, что светло-голубые и светло-зеленые точки отличаются друг от друга незначительно. Они распространены по всей поверхности примерно одинаково, что говорит о схожести в них микробиома (см. обсуждение результатов в конце отчета). Отдельно выделена "аномальная" точка NED.


Рисунок [baikal] Классификация мест отбора проб.


<!--
Principal Coordinates (PCoA) стремиться представить основные различия при помощи минимального количества атрибутов.

```bash
pcoa(phylip=  .opti_mcc.thetayc.0.03.lt.ave.dist)
```

В результате получаем три файла ```dist```, ```pcoa``` и ```pcoa.loadings```.  The stability.an.thetayc.0.03.lt.ave.pcoa.loadings file will tell you what fraction of the total variance in the data are represented by each of the axes. In this case, the first and second axis represent about 45 and 14% of the variation (59% of the total) for the thetaYC distances. The output to the screen indicates that the R-squared between the original distance matrix and the distance between the points in 2D PCoA space was 0.89, but that if you add a third dimension the R-squared value increases to 0.98. All in all, not bad.
-->

Воспользуемся вторым способом оценки схожести, называемым является "Неметрическое многомерное шкалирование" (Non-metric multidimensional scaling, NMDS). Метод пытается вычислить "расстояние" между образцами, используя указанное пользователем число измерений. Пропустим наши данные через команду NMDS, указав 2 измерения:

```bash
nmds(phylip=HXH779K01.tx.1.subsample.thetayc.1.lt.ave.dist)
```

<!--
HXH779K01.tx.1.subsample.thetayc.1.lt.ave.nmds.iters
HXH779K01.tx.1.subsample.thetayc.1.lt.ave.nmds.stress
HXH779K01.tx.1.subsample.thetayc.1.lt.ave.nmds.axes
-->

Получаем, помимо информации о различии характеристик проб на основе встрачаемости микроорганизмов, а также два коэффициента: Stress = 0.184, который должен быть меньше, и R^=0.853, который считается хорошим, если близок к 1. Можно посмотреть, что будет, если мы используем три измерения:

```bash
nmds(phylip=HXH779K01.tx.1.subsample.thetayc.1.lt.ave.dist, mindim=3, maxdim=3)
```

Теперь коэффициент R^2=0.93 увеличился, улучшился, а Stress=0.106 (должен быть меньше 0.20) уменьшился. Суммарный результат улучшился. Будем строить таблицы по этим данным.

<!--
HXH779K01.tx.1.subsample.thetayc.1.lt.ave.nmds.iters
HXH779K01.tx.1.subsample.thetayc.1.lt.ave.nmds.stress
HXH779K01.tx.1.subsample.thetayc.1.lt.ave.nmds.axes
-->

<!--
nmds<-read.table(file="nmds.axes", header=T)
rownames(nmds)<- nmds[,1]
sample_names.nmds<-factor(rownames(nmds))
plot(nmds$axis2~nmds$axis1, xlab="Axis 1", ylab="Axis 2")
with(nmds, text(x = axis1, y = axis2, labels = sample_names.nmds))
-->

Используя полученные данные, мы можем теперь построить 2-х-мерную диаграмму схожести/различия проб друг с другом. По горизонтали (Axis1) показано расположение проб по наиболее изменяющейся характеристике (OTU001), вторая координата дополняет первую. Эти две координаты достаточно, чтобы показать отличия с достоверностью 85%. По первой координате значение меняется от -0.25 до 0.45 (на 0.7), по второй от -0.2 до 0.2 (на 0.4). Диаграмма должна показывать как группируются пробы, но в данном случае явно мы это не наблюдаем, т.е. пробы не могут быть классифицированы явным образом.

Рисунок [beta]. Результаты анализа Бета-разнообразия.

<!-- Диаграмма -->

Again, it is clear that the early and late samples cluster separately from each other. Ultimately, ordination is a data visualization tool. We might ask if the spatial separation that we see between the early and late plots in the NMDS plot is statistically significant. To do this we have two statistical tools at our disposal. The first analysis of molecular variance (amova), tests whether the centers of the clouds representing a group are more separated than the variation among samples of the same treatment. This is done using the distance matrices we created earlier and does not actually use ordination.

<!--
We can test to determine whether the clustering within the ordinations is statistically significant or not using by using the amova command. To run amova, we will first need to create a design file that indicates which treatment each sample belongs to. There is a file called mouse.time.design in the folder you downloaded that looks vaguely like this:
-->

<!--
К этому моменту у нас есть данные, показывающие различие между пробами. Различие представляется наличием и количеством (?) OTU в пробах.
-->


# Отображение филогенетических деревьев


 <<Здесь вставка "Overview of Phylogenetic Tree Construction">>

Янь Чань Ю разработал пакет ```ggtree``` [CCY2017] для отображения филогенетических деревьев [CCY2020], позволяющий строить деревья различного вида (плоские, круговые), а также снабжать эти деревья дополнительной информацией, например, соотношениями встречаемости микроорганизмов в пробах. Пакет функционирует на платформе R, широко используемый учеными всего мира для обработки, анализа и визуализации данных.


Для использования пакета ```ggtree``` в нашей задаче необходимо произвести стыковку данных, полученных в результате обработки программой Mothur с ```ggtree```. Для этого необходимо подсчитать количество последовательностей по каждому OTU, разбить OTU на группы и организовать из этих групп филогенетичекую иерархию.

Построение деревьев осуществляется над объектами ```treedata```, которые получаются после загрузки данных из специальных файлов, и их преобразования в этот объект.

# Представление результатов исследования

Подтверждение и опровержение гипотез нашего исследования основывается на интерпретации результатов анализа. Мы провели фильтрацию последовательностей, их выравнивание относительно последовательностей в базе данных SILVA, построили множество уникальных последовательностей. Построили филогенетическое дерево (рисунок phyltree), диаграммы, отражающие Альфа-разнообразие (характеристика микробиома во всех местах отбора проб) (рисунки XX-YY) на основных OTU и OTU, которые удалось определить до вида, а также диаграмму, показывающую Бета-разнообразие (характеристика отличия мест отбора проб друг от друга) (рисунок beta).

Филогенетическое дерево на рисунке phyltree показывает в общем виде видовой состав микробиома, мутации генов (структура дерева), принадлежность к конкретным OTU (раскраска ребер). Для подтверждения второй гипотезы мы проанализировали филогенетическое дерево и среди OTU, классифицированных до вида и встречающихся чаще всего (наибольшее количество прочтений), мы обнаружили следующие виды бактерий: ..... OTU, имеющие максимальную встречаемость, но не определенные по базе данных, вероятно относятся к эндемичным видам бактерий Байкала, информации о которых нет в базе данных SILVA.

Среди обнаруженных видов бактерий, мы обнаружили бактерии, которые обитают в морской воде, а именно, ... Таким образом, вторая гипотеза подтверждается.

Для подтверждения или опровержения первой гипотезы проведем интерпретацию результатов Альфа- и Бета-разнообразия. Первый (Альфа) показал, что преобладающие виды идентифицированных бактерий встречаются во всех пробах. То же самое можно сказать про OTU001 ("эндемик"). Бактерии, находящиеся в пробах в малом количестве (за исключением Flavobacterium и OTU002 (Acidobacteria.5)), менее 5%, находятся в малом количестве во всех пробах. В целом заметного разнообразия и межвидовой корреляции не наблюдается. Пробы были отобраны на Байкале весной, когда он освободился ото льда. Температура фотического слоя в это время соответствует максимальной плотности воды - примерно 3 градуса Цельсия. Известный факт - вода Байкала перемешивается, особенно в зимний и весенний периоды, когда ее температура минимальна: при охлаждении воды на границе ледяного покрова до 3 градусов Цельсия, плотность воды-максимальна, и такая вода вытесняется более теплой или холодной (менее плотными). Это - основной фактор, обеспечивающий кислородом всю толщу Байкала, и, следовательно, жизнедеятельность живых организмов. В нашем случае это говорит о том, что бактерии находятся в постоянном движении, что вероятно, приводит к "выравниванию" видового разнообразия. Байкал находится внутри континента и весной практически всегда светит солнце. Байкал расположен между 51-м и 56-м градусом северной широты, и угол падения излучения солнца отличается в южной части от северной всего на 5 градусов, что также несущественно. Поэтому условия среды обитания можно считать одинаковыми. Единственное влияние на показания может оказать врем взятия пробы (угол падения солнечной радиации).

Если все-таки посмотреть на имеющиеся различия по численности бактерий в пробах по идентифицированным OTU и OTU001, то в для OTU001 имеем следующую картину: условия обитания OTU001 делятся на два класса. Первый - пробы SKM, SLT, STS, NBT, MAS, S3T, N3D, NZS, NKA, MAr, S3L, S3M, MMse, S3S, M3T, S12K, Ms (OTU001 содержится "много"), куда входят пробы, расположенные по всему озеру, но преимущественно в северной и южной оконечностях, а также по берегам. Остальные пробы - в основном, центральная часть Байкала. Может отличия среды обитания характеризуются с временем отбора проб или интенсивностью перемешивания в этих точках.

Встречаемость Flavobacterium и Acidobacteria.5 разделяет пробы на три класса: Chb, N3B, NED, где много Flavobacterium; NTN, MKKh, N3T, где много Acidobacteria.5; остальные места отбора проб, где этих бактерий мало. Illumatobacter заметно больше в пробе MMse (северная часть пролива "Малое море"), однако в MS, который расположен в проливе южнее, ее уже заметно меньше. На юге, севере и в центральной части ее мало. На рисунке [genusalpha] Flavobacterium показывает предпочтение некоторым местам в северной части Байкала, но при этом значимой зависимости также не наблюдается (есть точки в этой области, где бактерии встречается мало).

Анализ рисунка beta также показывает, что явно выраженного группирования проб в группы на наблюдается: пробы равномерно расположены как по первой оси, так и по второй. В некоторой степени "заметные" отличия показываю пробы NTN с MKKh, Chb и NED. Однако ни все относятся к центральной и северной части Байкала. Таким образом, делаем вывод, что первая гипотеза не подтверждается: нам не удалось найти значимых отличий между условиями обитания микроорганизмов между местами отбора проб.

С другой стороны, можно сделать вывод, что для подтверждения/опровержения гипотезы надо было провести дополнительные отборы проб в летний период, когда температура воды поднимается выше 3 градусов, перемешивание несколько замедляется. Полученные нами результаты обсуждены с авторами статьи mikhailov2018, интерпретация результатов, в целом совпала.

# Выводы

# Литература

Kozich JJ, Westcott SL, Baxter NT, Highlander SK, Schloss PD. (2013): Development of a dual-index sequencing strategy and curation pipeline for analyzing amplicon sequence data on the MiSeq Illumina sequencing platform. Applied and Environmental Microbiology. 79(17):5112-20.

Roy R. Lederman. Homopolymer Length Filters. Handbook of Systemic Autoimmune Diseases. Technical Report YALEU/DCS/TR-1465. 2012.

Matsumoto A, Kasai H, Matsuo Y, Omura S, Shizuri Y, Takahashi Y, Ilumatobacter fluminis gen. nov., sp. nov., a novel actinobacterium isolated from the sediment of an estuary. / J. Gen Appl Microbiol. 2009. Jun; 55(3) p. 201-5.

G Yu, DK Smith, H Zhu, Y Guan, TTY Lam*. ggtree: an R package for visualization and annotation of phylogenetic trees with their covariates and other associated data. Methods in Ecology and Evolution. 2017, 8(1):28-36. doi: 10.1111/2041-210X.12628.

Guangchuang Yu, Data Integration,Phylogenetic Tree Visualization / Manipulation and Visualization of Phylogenetic Trees, Department of Bioinformatics, School of Basic Medical Sciences, Southern Medical University, 2020. https://yulab-smu.github.io/treedata-book/
